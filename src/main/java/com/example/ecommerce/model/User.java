package com.example.ecommerce.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class User {
    private int id;
    private String name;
    private String surname;
    private Date birthdate;
    private String email;
    private String adress;
}
