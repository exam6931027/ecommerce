package com.example.ecommerce.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class Product {
    private int id;
    private String name;
    private String desc;
    private double amount;
    private int remainCount;
    private String productDetails;

}
