package com.example.ecommerce.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class Cart {
    private int id;
    private String product;
    private int count;
    private  double total_amount;
}
