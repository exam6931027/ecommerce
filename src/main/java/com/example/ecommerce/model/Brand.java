package com.example.ecommerce.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@Builder
public class Brand {
    private int id;
    private String name;
    private String desc;




}
