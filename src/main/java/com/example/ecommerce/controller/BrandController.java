package com.example.ecommerce.controller;

import com.example.ecommerce.model.Brand;
import com.example.ecommerce.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;



@RequestMapping("/api")
@RestController

@RequiredArgsConstructor
public class BrandController {

    private final BrandService brandService;

    @GetMapping(path = "/hello")
    @ResponseStatus(HttpStatus.CREATED)
    public String getInfoPost(){
        return "Hello";
    }

}
