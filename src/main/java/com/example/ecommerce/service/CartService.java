package com.example.ecommerce.service;

import com.example.ecommerce.model.Brand;
import com.example.ecommerce.model.Cart;

public interface CartService {
    void allProductinCart(Cart cart);
    void addProductToCart(int id);
    void removeProductFromCart(int id);
    void buyProductInCart(int id);
}
