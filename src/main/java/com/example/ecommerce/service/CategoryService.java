package com.example.ecommerce.service;

import com.example.ecommerce.model.Category;

public interface CategoryService {
    void allCategory(Category category);
    void saveCategory(int id);
    void  findById(int id);
    void updateCategory(int id);

}
