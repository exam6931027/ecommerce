package com.example.ecommerce.service;

import com.example.ecommerce.model.Brand;
import org.springframework.stereotype.Service;


public interface BrandService {
    void allBrands(Brand brand);
    Brand saveBrand(int id);
    void findById(int id);
    void updateBrand(int id);

}
