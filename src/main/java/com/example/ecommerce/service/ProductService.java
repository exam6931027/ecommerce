package com.example.ecommerce.service;

import com.example.ecommerce.model.Product;

public interface ProductService {
    void allProductOrSeacrh(Product product);
    void saveProduct(int id);
    void  findById(int id);
    void  updateProduct(int id);
}
