package com.example.ecommerce.service.impl;

import com.example.ecommerce.model.Cart;
import com.example.ecommerce.service.CartService;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImp implements CartService {

    @Override
    public void allProductinCart(Cart cart) {

    }

    @Override
    public void addProductToCart(int id) {

    }

    @Override
    public void removeProductFromCart(int id) {

    }

    @Override
    public void buyProductInCart(int id) {

    }
}
